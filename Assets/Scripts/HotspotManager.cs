using UnityEngine;

public class HotspotManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _hotspots;
    [SerializeField] private GameObject[] _hotspotPannels;

    void Start()
    {
        Debug.Log("HotspotManager called");
    }

    private void FixedUpdate() 
    {
        RayClick();
    }
    public void DisplayHotspotPannel(int index)
    {
        _hotspotPannels[index].SetActive(true);
        AudioManager.instance.TriggerAudioHotspots(0);
    }

    public void CloseHotspot(int index)
    {
        AudioManager.instance.TriggerAudioHotspots(0);
        for (int i = 0; i < _hotspotPannels.Length; i++)
        {
            index = i;
            _hotspotPannels[i].SetActive(false);
        }
    }

    public void RayClick()
    {
        Debug.Log("HotspotManager : RayClick");

        if (RaycastGenerator.instance.hit.transform != null)
        {
            switch (RaycastGenerator.instance.hit.transform.tag)
            {
                case "Hotspot1":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    DisplayHotspotPannel(0);
                    break;

                case "Hotspot2":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    DisplayHotspotPannel(1);
                    break;

                case "Hotspot3":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    DisplayHotspotPannel(2);
                    break;

                case "Hotspot4":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    DisplayHotspotPannel(3);
                    break;

                case "Hotspot5":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    DisplayHotspotPannel(4);
                    break;

                case "BackToMenu":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LevelManager.instance.LoadSceneOnClick(LevelManager.SceneType.SceneMenuRoomSelection);
                    break;

                case "Button Next Showroom":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LevelManager.instance.LoadSceneOnClick(LevelManager.SceneType.SceneShowroomHub2);
                    break;

                case "Button Back To Showroom":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LevelManager.instance.LoadSceneOnClick(LevelManager.SceneType.SceneShowroomHub1);
                    break;

                case "Close Hotspot":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    CloseHotspot(0);
                    CloseHotspot(1);
                    CloseHotspot(2);
                    CloseHotspot(3);
                    CloseHotspot(4);
                    break;

                default:
                    break;
            }
        }
    }
}
