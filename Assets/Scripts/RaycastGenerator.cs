using UnityEngine;

public class RaycastGenerator : MonoBehaviour
{
    public RaycastHit hit;
    public static RaycastGenerator instance;

    private void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start() 
    {
        Debug.Log("RaycastGenerator called");    
    }

    private void FixedUpdate() 
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
            Debug.Log("RaycastGenerator : Cursor _hit " + hit.transform.name);

            hit.collider.gameObject.SendMessage("RayClick", SendMessageOptions.DontRequireReceiver);
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
        }   
    }
}
