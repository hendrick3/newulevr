using System.Collections;
using UnityEngine;
using UnityEngine.Localization.Settings;

public class LocalizationManager : MonoBehaviour
{
    [SerializeField] private GameObject _frenchFlag;
    [SerializeField] private GameObject _englishFlag;

    private bool _active; // Check if the coroutine is called once
    private bool _hasChangedLanguage;

    private void Awake()
    {
        int id = PlayerPrefs.GetInt("LocalKey", 0);
        ChangeLocale(id); // Load the last locale id selected by the user    
    }

    private void Start() 
    {
        Debug.Log("LocalizationManager called");  
    }

    private void Update() 
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out RaycastGenerator.instance.hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * RaycastGenerator.instance.hit.distance, Color.red);

            if (RaycastGenerator.instance.hit.transform.CompareTag("Button French"))
            {
                Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                ChangeLocale(1);
            }
            else if (RaycastGenerator.instance.hit.transform.CompareTag("Button English"))
            {
                Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                ChangeLocale(0);
            }
        }
    }

    public void ChangeLocale(int localeID)
    {
        if (_active == true)
            return;

        StartCoroutine(SetLocale(localeID));
        StartCoroutine(StartExperienceAfterDelay());
    }

    private IEnumerator SetLocale(int localeID)
    {
        _active = true;

        DisplayLanguageFlag(localeID);

        // Wait until Localization is loaded and ready to use
        yield return LocalizationSettings.InitializationOperation;

        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[localeID];
        PlayerPrefs.SetInt("LocalKey", localeID); // Save the locale id number the user has chosen
        _active = false;
    }

    private IEnumerator StartExperienceAfterDelay()
    {
        yield return new WaitForSeconds(0.3f);

        if (_active == true)
        {
            LevelManager.instance.LoadSceneOnClick(LevelManager.SceneType.SceneVideoIntroduction);
        }
    }

    private void DisplayLanguageFlag(int localeID)
    {
        if (localeID == 0)
        {
            _englishFlag.SetActive(true);
            _frenchFlag.SetActive(false);
        }
        else if (localeID == 1)
        {
            _englishFlag.SetActive(false);
            _frenchFlag.SetActive(true);
        }
        else
        {
            return;
        }
    }
}