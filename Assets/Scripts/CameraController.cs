using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float sensitivity = 100f; // Sensibilité de la souris
    public Transform player; // Transform du joueur (pour verrouiller la rotation verticale)

    private float xRotation = 0f;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; // Verrouille le curseur au centre de l'écran
    }

    void Update()
    {
        // Get the mouse mouvements
        float mouseX = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;

        // Tourne la caméra horizontalement
        transform.Rotate(Vector3.up, mouseX);

        // Tourne la caméra verticalement (en limitant l'angle de rotation)
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        // Verrouille la rotation verticale de la caméra sur l'axe Y du joueur
        player.Rotate(Vector3.up, mouseX);
    }
}
