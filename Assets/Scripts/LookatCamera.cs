using UnityEngine;

public class LookatCamera : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;

   private void LateUpdate()
    {
        // Calculer la direction de la caméra par rapport à l'objet
        Vector3 direction = mainCamera.transform.position - transform.position;
        // Ajuster la direction pour prendre en compte l'orientation de l'objet
        Vector3 adjustedDirection = new Vector3(direction.x, 0, direction.z);
        // Faire en sorte que l'objet regarde vers la caméra
        transform.rotation = Quaternion.LookRotation(adjustedDirection);
    }
}
