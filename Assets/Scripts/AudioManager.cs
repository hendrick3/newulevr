using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [SerializeField] private AudioSource _audioSourceGeneral;
    [SerializeField] private AudioSource _audioSourceHotspots;

    [SerializeField] private AudioClip[] _audioClipGenerals;
    [SerializeField] private AudioClip[] _audioHotspots;


    private void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }   
    }

    private void Start() 
    {
        Debug.Log("AudioManager called");    
    }

    public void TriggerAudioGeneral(int index)
    {
        Debug.Log("AudioManager : TriggerAudioGeneral called");    

        _audioSourceGeneral.clip = _audioClipGenerals[index];
        _audioSourceGeneral.volume = 0.6f;
        _audioSourceGeneral.Play();
    }

    public void TriggerAudioHotspots(int index)
    {
        Debug.Log("AudioManager : TriggerAudioHotspots called");    

        _audioSourceHotspots.clip = _audioHotspots[index];
        _audioSourceHotspots.volume = 0.6f;
        _audioSourceHotspots.Play();
    }
}
