using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    private int _indexAudio;
    private bool _activeCoroutine;


    public enum SceneType
    {
        SceneLanguageSelection,
        SceneVideoIntroduction,
        SceneMenuRoomSelection,
        SceneShowroomHub1,
        SceneShowroomHub2,
        ScenePepiniereHub,
        SceneProductionHub1,
        SceneProductionHub2,
        SceneUniteTech
    }

    private void Awake() 
    {
        // Check if the instance exist, if it does not we create it, if does we destroy it 
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else 
        {
            Destroy(this);
        }
    }

    private void Start() 
    {
        Debug.Log("LevelManager called");
    }

    private void FixedUpdate() 
    {
        if (RaycastGenerator.instance.hit.transform != null)
        {
            switch (RaycastGenerator.instance.hit.transform.tag)
            {
                case "Button Next":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LoadSceneOnClick(SceneType.SceneMenuRoomSelection);
                   break;

                case "Button Pepiniere":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LoadSceneOnClick(SceneType.ScenePepiniereHub);
                   break;

                case "Button UTech":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LoadSceneOnClick(SceneType.SceneUniteTech);
                   break;

                case "Button Showroom":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LoadSceneOnClick(SceneType.SceneShowroomHub1);
                   break;

                case "Button Production":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LoadSceneOnClick(SceneType.SceneProductionHub1);
                   break;

                case "BackToMenu":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LoadSceneOnClick(SceneType.SceneMenuRoomSelection);
                   break;

                case "BackToLangueSelection":
                    Debug.Log("Cursor hit " + RaycastGenerator.instance.hit.transform.name);
                    LoadSceneOnClick(SceneType.SceneLanguageSelection);
                   break;

                default:
                   break;
            }
        }
    }
    
    // Logic for scenes management
    private IEnumerator LoadSceneAfterDelayCoroutine(SceneType sceneToLoad)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad.ToString());
        AudioManager.instance.TriggerAudioHotspots(0);

        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        LoadSceneOnGazeCursor(sceneToLoad);
    }

    // Methods to call to load scene 
    public void LoadSceneOnGazeCursor(SceneType sceneToLoad)
    {
        Debug.Log("LoadSceneOnGazeCursor");
        StartCoroutine(LoadSceneAfterDelayCoroutine(sceneToLoad));
    }

    public void LoadSceneType(SceneType sceneToLoad)
    {
        LoadSceneOnGazeCursor(sceneToLoad);
    }

     public void LoadSceneOnClick(SceneType sceneToLoad)
    {
        if (_activeCoroutine == true)
            return;

        AudioManager.instance.TriggerAudioHotspots(0);
        StartCoroutine(LoadSceneAfterAudioOnClickCoroutine(sceneToLoad));
    }

    private IEnumerator LoadSceneAfterAudioOnClickCoroutine(SceneType sceneToLoad)
    {
        _activeCoroutine = true;
        yield return new WaitForSeconds(0.2f);
        LoadSceneType(sceneToLoad);
        _activeCoroutine = false;
    }
}
